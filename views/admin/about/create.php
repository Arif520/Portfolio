<?php
include_once '../includes/header.php';
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Home Section Here
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="views/admin/index.php"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">About</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">


        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">

                <div class="col-md-12">

                    <!-- Horizontal Form -->
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Home Section</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" action="views/admin/about/store.php" method="post" enctype="multipart/form-data">

                            <div class="box-body">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name" class="col-sm-2 control-label">Name</label>

                                        <div class="col-sm-10">
                                            <input type="text" name="name" class="form-control" id="name" placeholder="Name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="age" class="col-sm-2 control-label">Age</label>

                                        <div class="col-sm-10">
                                            <input type="number" name="age" class="form-control" id="age" placeholder="Age">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="address" class="col-sm-2 control-label">Address</label>

                                        <div class="col-sm-10">
                                            <input type="text" name="address" class="form-control" id="address" placeholder="Address">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="email" class="col-sm-2 control-label">Email</label>

                                        <div class="col-sm-10">
                                            <input type="text" name="email" class="form-control" id="email" placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="phone" class="col-sm-2 control-label">Phone</label>

                                        <div class="col-sm-10">
                                            <input type="text" name="phone" class="form-control" id="phone" placeholder="Phone">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="website" class="col-sm-2 control-label">Website</label>

                                        <div class="col-sm-10">
                                            <input type="text" name="website_url" class="form-control" id="website" placeholder="http://">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="file" class="col-sm-2 control-label">CV</label>

                                        <div class="col-sm-10">
                                            <input type="file" name="cvfile" class="form-control" id="file">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="file" class="col-sm-2 control-label">Image</label>

                                        <div class="col-sm-10">
                                            <input type="file" name="imagefile" class="form-control" id="file">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label>Twitter:</label>

                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-twitter"></i>
                                            </div>
                                            <input type="text" class="form-control" name="twitter_url" placeholder="http://">
                                        </div>
                                        <!-- /.input group -->
                                    </div>

                                    <div class="form-group">
                                        <label>Linkedin:</label>

                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-linkedin"></i>
                                            </div>
                                            <input type="text" class="form-control" name="linkedin_url" placeholder="http://">
                                        </div>
                                        <!-- /.input group -->
                                    </div>

                                    <div class="form-group">
                                        <label>Facebook:</label>

                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-facebook"></i>
                                            </div>
                                            <input type="text" class="form-control" name="facebook_url" placeholder="http://">
                                        </div>
                                        <!-- /.input group -->
                                    </div>

                                    <div class="form-group">
                                        <label>Github:</label>

                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-github"></i>
                                            </div>
                                            <input type="text" class="form-control" name="github_url" placeholder="http://">
                                        </div>
                                        <!-- /.input group -->
                                    </div>

                                    <div class="form-group">
                                        <label>Skype:</label>

                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-skype"></i>
                                            </div>
                                            <input type="text" class="form-control" name="skype_url" placeholder="http://">
                                        </div>
                                        <!-- /.input group -->
                                    </div>

                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-default">Cancel</button>
                                        <button type="submit" class="btn btn-info pull-right">Submit</button>
                                    </div>
                                    <!-- /.box-footer -->
                                    <div>

                                    </div>
                                </div>
                                <!-- /.row -->
                                <!-- Main row -->


        </section>
        <!-- /.content -->
</div>
<!-- /.content-wrapper -->



<?php
include_once '../includes/footer.php';
?>
