<?php
include_once 'includes/header.php';
?>
	<body>
		<!-- Header -->
		<header class="header header-hidden z-depth-1 shadow-change">
			<div class="site-logo"><a href="#0">TILE</a></div>
			<div class="menu-bar btn-floating waves-effect waves-light"><span class="fa fa-bars"></span></div>
			<div class="search-open btn-floating waves-effect waves-light"><span class="fa fa-search"></span></div>
			<div class="search-area search-area-hidden clearfix">
				<div class="search-input">
					<form>
						<fieldset>
							<input type="search" placeholder="Type Here & Hit Enter...">
						</fieldset>
					</form>
				</div>
			</div>
			<nav class="main-nav">
				<ul>
					<li><a href="#0" class="animatescroll-link waves-effect" onclick="$('html').animatescroll();">Home</a></li>
					<li><a href="#0" class="skill-section-nav animatescroll-link waves-effect" onclick="$('#skill-section').animatescroll();">Skills</a></li>
					<li><a href="#0" class="education-section-nav animatescroll-link waves-effect" onclick="$('#education-section').animatescroll();">Education</a></li>
					<li><a href="#0" class="experience-section-nav animatescroll-link waves-effect" onclick="$('#experience-section').animatescroll();">Experience</a></li>
					<li><a href="#0" class="portfolio-section-nav animatescroll-link waves-effect" onclick="$('#portfolio-section').animatescroll();">Portfolio</a></li>
					<li><a href="#0" class="testimonial-section-nav animatescroll-link waves-effect" onclick="$('#testimonial-section').animatescroll();">Testimonial</a></li>
					<li><a href="#0" class="blog-section-nav animatescroll-link waves-effect" onclick="$('#blog-section').animatescroll();">Blog</a></li>
					<li><a href="#0" class="contact-section-nav animatescroll-link waves-effect" onclick="$('#contact-section').animatescroll();">Contact</a></li>
				</ul>
			</nav>
		</header>
		<!--/ Header -->

		<!-- Top Section -->
		<div class="site-header z-depth-1 top-section">
			<div class="container">
				<div class="row">
					<div class="col l6 m6 s12 pd-0">
						<div class="site-header-title">
							MD Arifur Rahman
							<span>Software Engineer</span>
						</div>
					</div>
					<div class="col l6 m6 s12 pd-0">
						<div class="site-header-contact">
							<a class="btn btn-floating waves-effect waves-light" href="#0"><span class="fa fa-facebook"></span></a>
							<a class="btn btn-floating waves-effect waves-light" href="#0"><span class="fa fa-twitter"></span></a>
							<a class="btn btn-floating waves-effect waves-light" href="#0"><span class="fa fa-google-plus"></span></a>
							<a class="btn btn-floating waves-effect waves-light" href="#0"><span class="fa fa-linkedin"></span></a>
							<a class="btn btn-floating waves-effect waves-light" href="#0"><span class="fa fa-github"></span></a>
							<a class="btn btn-floating waves-effect waves-light" href="#0"><span class="fa fa-skype"></span></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/ Top Section -->

		<!-- Single Blog Detail Section -->
		<div class="single-blog-section">
			<div class="container">
				<div class="row">
					<div class="col l8 m8 s12 single-blog-wrapper pdl-0">
						<div class="col s12 w-block z-depth-1 shadow-change pd-50">
							<div class="single-blog-title">
								Sigle blog post title with large sweet font
							</div>
							<div class="single-blog-data blog-data">
								<a href="#0" class="waves-effect"><span class="fa fa-user"></span>John Doe</a>
								<a href="#0" class="waves-effect"><span class="fa fa-calendar"></span>Apr 7</a>
								<a href="#0" class="waves-effect"><span class="fa fa-heart"></span>32</a>
							</div>
							<div class="single-blog-content">
								<p>
									Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue.
								</p>
								<img src="assets/font-end/img/blog/large/bl-1.jpg" alt="Image"/>
								<p>
									Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular. Li lingues differe solmen in li grammatica, li pronunciation e li plu commun vocabules. Omnicos directe al desirabilite de un nov lingua franca: On refusa continuar payar custosi traductores. At solmen va esser necessi far uniform grammatica, pronunciation e plu sommun paroles.
								</p>
								<blockquote>
									Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.
								</blockquote>
								<p>
									Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
								</p>
							</div>
							<div class="single-blog-navigation">
								<a href="#0" class="btn waves-effect waves-light">Prev Post</a>
								<a href="#0" class="btn waves-effect waves-light">Next Post</a>
							</div>
						</div>
						<div class="col s12 single-blog-comment w-block z-depth-1 shadow-change pd-50 mgt-50">
							<div class="comment-title">
								Comments(12)
							</div>
							<div class="col s12 comment-content pd-0">
								<div class="comment-img">
									<img src="assets/font-end/img/blog/commenter/c-1.jpg" alt="Commenter"/>
								</div>
								<div class="comment-data">
									<a href="#0">John Doe</a>
									<span>May 10, 2016</span>
								</div>
								<div class="comment-desc">
									<p>
										Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
									</p>
								</div>
								<div class="comment-reply">
									<a href="#0"><span class="fa fa-reply"></span>Reply</a>
								</div>
								<div class="sub-comment-wrapper">
									<div class="col s11 offset-s1 comment-content pd-0">
										<div class="comment-img">
											<img src="assets/font-end/img/blog/commenter/c-2.jpg" alt="Commenter"/>
										</div>
										<div class="comment-data">
											<a href="#0">John Doe</a>
											<span>May 10, 2016</span>
										</div>
										<div class="comment-desc">
											<p>
												Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
											</p>
										</div>
										<div class="comment-reply">
											<a href="#0"><span class="fa fa-reply"></span>Reply</a>
										</div>
									</div>
								</div>
							</div>
							<div class="col s12 comment-content pd-0">
								<div class="comment-img">
									<img src="assets/font-end/img/blog/commenter/c-3.jpg" alt="Commenter"/>
								</div>
								<div class="comment-data">
									<a href="#0">John Doe</a>
									<span>May 10, 2016</span>
								</div>
								<div class="comment-desc">
									<p>
										Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
									</p>
								</div>
								<div class="comment-reply">
									<a href="#0"><span class="fa fa-reply"></span>Reply</a>
								</div>
							</div>
							<div class="col s12 comment-form pd-0">
								<form id="comment-form" class="comment-form" action="#0" method="post">
									<fieldset>
										<div class="input-field">
											<input id="name" type="text" name="name" class="validate">
											<label for="name">Name</label>
										</div>
										<div class="input-field">
											<input id="email" type="email" name="email" class="validate">
											<label for="email">Email</label>
										</div>
										<div class="input-field">
											<textarea id="message" name="message" class="materialize-textarea validate"></textarea>
											<label for="message">Comment</label>
										</div>
										<div>
											<button class="btn waves-effect waves-light" type="submit" name="button">Post Comment</button>
										</div>
									</fieldset>
								</form>
							</div>
						</div>
					</div>
					<div class="col l4 m4 s12 sidebar-wrapper pdr-0">
						<div class="col s12 single-sidebar w-block z-depth-1 shadow-change pd-30">
							<div class="sidebar-title">
								Author
							</div>
							<div class="author-content">
								<img src="assets/font-end/img/me.jpg" alt="Author Image"/>
								<div class="author-data">
									 MD Arifur Rahman
									<span>Software Engineer</span>
								</div>
								<p>
									Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
								</p>
							</div>
						</div>
						<div class="col s12 single-sidebar w-block z-depth-1 shadow-change pdt-30 pdr-30 pdb-40 pdl-30">
							<div class="sidebar-title">
								Related Post
							</div>
							<div class="related-post-content">
								<div class="related-post-single">
									<div class="related-post-img">
										<img src="assets/font-end/img/blog/thumb/bt-1.jpg" alt="Blog Thumbnail"/>
									</div>
									<div class="related-post-title">
										<a href="#0">Related Post Title In Sidebar</a>
									</div>
									<div class="related-post-author">
										<a href="#0"><span class="fa fa-user"></span>John Doe</a>
									</div>
								</div>
								<div class="related-post-single">
									<div class="related-post-img">
										<img src="assets/font-end/img/blog/thumb/bt-2.jpg" alt="Blog Thumbnail"/>
									</div>
									<div class="related-post-title">
										<a href="#0">Related Post Title In Sidebar</a>
									</div>
									<div class="related-post-author">
										<a href="#0"><span class="fa fa-user"></span>John Doe</a>
									</div>
								</div>
								<div class="related-post-single">
									<div class="related-post-img">
										<img src="assets/font-end/img/blog/thumb/bt-3.jpg" alt="Blog Thumbnail"/>
									</div>
									<div class="related-post-title">
										<a href="#0">Related Post Title In Sidebar</a>
									</div>
									<div class="related-post-author">
										<a href="#0"><span class="fa fa-user"></span>John Doe</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col s12 single-sidebar w-block z-depth-1 shadow-change pd-30">
							<div class="sidebar-title">
								Category
							</div>
							<div class="category-content">
								<a href="#0">Data Structure</a>
								<a href="#0">Algorithm</a>
								<a href="#0">Discrete Math</a>
								<a href="#0">Data Analysis</a>
								<a href="#0">Machine Learning</a>
								<a href="#0">Artificial Intelligece</a>
							</div>
						</div>
						<div class="col s12 single-sidebar w-block z-depth-1 shadow-change pd-30">
							<div class="sidebar-title">
								Archive
							</div>
							<div class="archive-content">
								<a href="#0">May - 2016</a>
								<a href="#0">April - 2016</a>
								<a href="#0">March - 2016</a>
								<a href="#0">February - 2016</a>
								<a href="#0">January - 2016</a>
								<a href="#0">December - 2015</a>
								<a href="#0">November - 2015</a>
								<a href="#0">October - 2015</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/ Single Blog Detail Section -->

		<!-- Footer Section -->
		<footer class="footer">
			<div class="container">
				<div class="row">
					<div class="col s12">
						<a class="btn btn-floating waves-effect waves-light back-to-top animatescroll-link" onclick="$('html').animatescroll();" href="#0">
							<span class="fa fa-angle-up"></span>
						</a>
						<div class="social-links">
							<a class="waves-effect waves-light" href="#0"><span class="fa fa-facebook"></span></a>
							<a class="waves-effect waves-light" href="#0"><span class="fa fa-twitter"></span></a>
							<a class="waves-effect waves-light" href="#0"><span class="fa fa-google-plus"></span></a>
							<a class="waves-effect waves-light" href="#0"><span class="fa fa-linkedin"></span></a>
							<a class="waves-effect waves-light" href="#0"><span class="fa fa-github"></span></a>
							<a class="waves-effect waves-light" href="#0"><span class="fa fa-skype"></span></a>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!--/ Footer Section -->

		<!-- Color Scheme -->
		<div class="color-scheme-select">
			<div class="color-scheme-title">
				20 Awesome Colors
			</div>
			<div id="color-1" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#0EB57D"></div>
			<div id="color-2" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#2196F3"></div>
			<div id="color-3" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#FF1902"></div>
			<div id="color-4" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#FF9800"></div>
			<div id="color-5" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#E91E63"></div>
			<div id="color-6" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#009688"></div>
			<div id="color-7" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#FF5722"></div>
			<div id="color-8" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#9EC139"></div>
			<div id="color-9" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#9C27B0"></div>
			<div id="color-10" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#4CAF50"></div>
			<div id="color-11" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#795548"></div>
			<div id="color-12" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#FF007F"></div>
			<div id="color-13" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#673AB7"></div>
			<div id="color-14" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#8BC34A"></div>
			<div id="color-15" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#3E2723"></div>
			<div id="color-16" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#FF7711"></div>
			<div id="color-17" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#BF9C4F"></div>
			<div id="color-18" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#33691E"></div>
			<div id="color-19" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#607D8B"></div>
			<div id="color-20" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#FF7077"></div>
			<div class="color-scheme-select-btn">
				<span class="fa fa-cog"></span>
			</div>
		</div>
		<!-- Color Scheme -->

     <?php
        include_once 'includes/footer.php';
     ?>
