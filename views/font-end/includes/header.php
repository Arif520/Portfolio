<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta charset="UTF-8">
    <meta name="author" content="Md. Junaid Khan Pathan">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Engr-Arifur</title>
    <link rel="shortcut icon" href="assets/font-end/img/favicon.png"/>

    <base href="http://localhost/myProtfolio/">

    <link rel="apple-touch-icon" href="assets/font-end/img/apple-touch-icon.png"/>
    <link rel="apple-touch-icon" sizes="72x72" href="assets/font-end/img/apple-touch-icon-72x72.png"/>
    <link rel="apple-touch-icon" sizes="114x114" href="assets/font-end/img/apple-touch-icon-114x114.png"/>
    <link rel="apple-touch-icon" sizes="144x144" href="assets/font-end/img/apple-touch-icon-144x144.png"/>
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300italic,300,900italic,900,700italic,700,500italic,500,400italic">
    <link type="text/css" rel="stylesheet" media="all" href="assets/font-end/font/font-awesome/css/font-awesome.min.css"/>
    <link type="text/css" rel="stylesheet" media="all" href="assets/font-end/css/animate.css">
    <link type="text/css" rel="stylesheet" media="all" href="assets/font-end/css/materialize.min.css">
    <link type="text/css" rel="stylesheet" media="all" href="assets/font-end/css/magnific-popup.css">
    <link type="text/css" rel="stylesheet" media="all" href="assets/font-end/css/owl.carousel.css">
    <link type="text/css" rel="stylesheet" media="all" href="assets/font-end/css/owl.theme.css">
    <link type="text/css" rel="stylesheet" media="all" href="assets/font-end/css/owl.transitions.css">
    <link type="text/css" rel="stylesheet" media="all" href="assets/font-end/css/style.css">
    <link class="color-scheme" type="text/css" rel="stylesheet" media="all" href="assets/font-end/css/color-1.css">
    <link type="text/css" rel="stylesheet" media="all" href="assets/font-end/css/responsive.css">
    <script type="text/javascript" src="assets/font-end/js/modernizr.js"></script>
</head>