-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 14, 2018 at 08:11 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `portfolio`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `age` int(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` varchar(111) NOT NULL,
  `website_url` varchar(50) NOT NULL,
  `cvfile` varchar(100) NOT NULL,
  `imagefile` varchar(100) NOT NULL,
  `twitter_url` varchar(50) NOT NULL,
  `linkedin_url` varchar(50) NOT NULL,
  `facebook_url` varchar(50) NOT NULL,
  `github_url` varchar(50) NOT NULL,
  `skype_url` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `name`, `age`, `phone`, `email`, `address`, `website_url`, `cvfile`, `imagefile`, `twitter_url`, `linkedin_url`, `facebook_url`, `github_url`, `skype_url`) VALUES
(2, 'Arif', 26, '01703930610', 'aparifurrahman@gmail.com', 'Lalkhan Bazar', 'arifur.bd.com', '24ab61dae5.pdf', '24ab61dae5.jpg', 'https://twitter.com/aparifurrahman', 'https://www.linkedin.com/in/md-arifur-rahman-2905a', 'https://www.facebook.com/Arif.IIUC', 'https://gitlab.com/Arif520', 'arifur205'),
(3, 'Engr.MD Arifur Rahman', 0, '', '', 'Lalkhan Bazar , Chittagong', '', '9970f07293.', '9970f07293.', '', '', '', '', ''),
(4, 'Engr.MD Arifur Rahman', 25, '01703930610', 'aparifurrahman@gmail.com', 'Lalkhan Bazar , Chittagong', 'arifur.bd.com', '05e2e1cf37.pdf', '05e2e1cf37.jpg', 'https://twitter.com/aparifurrahman', 'https://www.linkedin.com/in/md-arifur-rahman-2905a', 'https://www.facebook.com/Arif.IIUC', 'https://gitlab.com/Arif520', 'arifur205');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
