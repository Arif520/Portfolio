<?php

namespace App\admin\About;

use App\Database\connection;

use PDO;

class About extends connection
{
    public $name ='';
    public $age ='';
    public $address ='';
    public $phone ='';
    public $email ='';
    public $website_url ='';
    public $cvfile ='';
    public $imagefile ='';
    public $twitter_url ='';
    public $linkedin_url ='';
    public $facebook_url ='';
    public $github_url ='';
    public $skype_url ='';

    public function set($data = array()) // type hinting ...
    {
        if (array_key_exists('name', $data)) {
            $this->name = $data['name'];
        }
        if (array_key_exists('age', $data)) {
            $this->age = $data['age'];
        }
        if (array_key_exists('address', $data)) {
            $this->address = $data['address'];
        }
        if (array_key_exists('phone', $data)) {
            $this->phone = $data['phone'];
        }
        if (array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists('website_url', $data)) {
            $this->website_url = $data['website_url'];
        }
        if (array_key_exists('cvfile', $data)) {
            $this->cvfile = $data['cvfile'];
        }
        if (array_key_exists('imagefile', $data)) {
            $this->imagefile = $data['imagefile'];
        }
        if (array_key_exists('twitter_url', $data)) {
            $this->twitter_url = $data['twitter_url'];
        }
        if (array_key_exists('linkedin_url', $data)) {
            $this->linkedin_url = $data['linkedin_url'];
        }
        if (array_key_exists('facebook_url', $data)) {
            $this->facebook_url = $data['facebook_url'];
        }
        if (array_key_exists('github_url', $data)) {
            $this->github_url = $data['github_url'];
        }
        if (array_key_exists('skype_url', $data)) {
            $this->skype_url = $data['skype_url'];
        }
        return $this;

    }

    public function store(){

        try {

            $stm = $this->db->prepare("INSERT INTO `about` (name, age, phone, email, address, website_url, cvfile, imagefile, twitter_url, linkedin_url,facebook_url,github_url,skype_url)
                                     VALUES 
                                    (:name, :age, :phone, :email, :address, :website_url, :cvfile, :imagefile, :twitter_url, :linkedin_url, :facebook_url, :github_url, :skype_url)");

            $data =  $stm->execute(array(
                ':name' => $this->name,
                ':age' => $this->age,
                ':phone' => $this->phone,
                ':email' => $this->email,
                ':address' => $this->address,
                ':website_url' => $this->website_url,
                ':cvfile' => $this->cvfile,
                ':imagefile' => $this->imagefile,
                ':twitter_url' => $this->twitter_url,
                ':linkedin_url' => $this->linkedin_url,
                ':facebook_url' => $this->facebook_url,
                ':github_url' => $this->github_url,
                ':skype_url' => $this->skype_url,

            ));
            if($data){
                header('location:index.php');
            }
        } catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }

    }

    public function index(){

        try{

            $sql = "SELECT * FROM `about`ORDER BY id DESC LIMIT 1";

            $stmt = $this->db->prepare($sql);

            $stmt->execute();

            return $stmt->fetch(PDO::FETCH_ASSOC);

        }
        catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }


}