<?php

namespace App\Database;

use PDO;

use PDOException;


class connection{

    protected $db;

    public $user="root";
    public $pass="";

    public function __construct()
    {
        try {

            # MySQL with PDO_MYSQL
            $this->db = new PDO("mysql:host=localhost;dbname=portfolio", $this->user, $this->pass);

        }
        catch(PDOException $e) {

            echo $e->getMessage();
        }
    }
}
