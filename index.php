<?php
 include_once 'views/font-end/includes/header.php';

include_once 'vendor/autoload.php';

use App\admin\About\About;

$object = new About();

$user = $object->index();

?>

<!-- Preloader -->

<!--/ Preloader -->
<body>
<!-- Header -->
<header class="header header-hidden z-depth-1 shadow-change">
    <div class="site-logo"><a href="#0"><?= $user['name']?></a></div>
    <div class="menu-bar btn-floating waves-effect waves-light"><span class="fa fa-bars"></span></div>
    <div class="search-open btn-floating waves-effect waves-light"><span class="fa fa-search"></span></div>
    <div class="search-area search-area-hidden clearfix">
        <div class="search-input">
            <form>
                <fieldset>
                    <input type="search" placeholder="Type Here & Hit Enter...">
                </fieldset>
            </form>
        </div>
    </div>
    <nav class="main-nav">
        <ul>
            <li><a href="#0" class="animatescroll-link waves-effect" onclick="$('html').animatescroll();">Home</a></li>
            <li><a href="#0" class="skill-section-nav animatescroll-link waves-effect" onclick="$('#skill-section').animatescroll();">Skills</a></li>
            <li><a href="#0" class="education-section-nav animatescroll-link waves-effect" onclick="$('#education-section').animatescroll();">Education</a></li>
            <li><a href="#0" class="experience-section-nav animatescroll-link waves-effect" onclick="$('#experience-section').animatescroll();">Experience</a></li>
            <li><a href="#0" class="portfolio-section-nav animatescroll-link waves-effect" onclick="$('#portfolio-section').animatescroll();">Portfolio</a></li>

       <!--
            <li><a href="#0" class="testimonial-section-nav animatescroll-link waves-effect" onclick="$('#testimonial-section').animatescroll();">Testimonial</a></li>
            <li><a href="#0" class="blog-section-nav animatescroll-link waves-effect" onclick="$('#blog-section').animatescroll();">Blog</a></li>

         -->

            <li><a href="#0" class="contact-section-nav animatescroll-link waves-effect" onclick="$('#contact-section').animatescroll();">Contact</a></li>
        </ul>
    </nav>
</header>
<!--/ Header -->

<!-- Top Section -->
<div class="site-header z-depth-1 top-section">
    <div class="container">
        <div class="row">
            <div class="col l6 m6 s12 pd-0">
                <div class="site-header-title">

                    <?= $user['name']?>

                    <span>Software Engineer</span>
                    <span></span>
                </div>
            </div>
            <div class="col l6 m6 s12 pd-0">
                <div class="site-header-contact">
                    <a class="btn btn-floating waves-effect waves-light" href="https://www.facebook.com/Arif.IIUC"><span class="fa fa-facebook"></span></a>
                    <a class="btn btn-floating waves-effect waves-light" href="https://twitter.com/aparifurrahman"><span class="fa fa-twitter"></span></a>
                    <a class="btn btn-floating waves-effect waves-light" href=><span class="fa fa-google-plus"></span></a>
                    <a class="btn btn-floating waves-effect waves-light" href="https://www.linkedin.com/in/md-arifur-rahman-2905a7158/"><span class="fa fa-linkedin"></span></a>
                    <a class="btn btn-floating waves-effect waves-light" href="https://gitlab.com/Arif520"><span class="fa fa-github"></span></a>
                    <a class="btn btn-floating waves-effect waves-light" href=><span class="fa fa-skype"></span></a>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/ Top Section -->

<!--data-image-bg="assets/font-end/img/me.jpg" -->

<!-- Overview Section -->
<section id="overview-section" class="overview-section">
    <div class="section-content">
        <div class="container">
            <div class="row">
                <div class="col s12 about-section w-block z-depth-1 shadow-change pd-0 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" data-wow-offset="0">
                    <div class="col l5 m6 s12 about-img pd-0 image-bg" data-image-bg="views/admin/uploads/<?= $user['imagefile']?>">
                        <div class="about-more">
                            <div class="about-more-content">
                                <a class="btn btn-floating btn-large tooltipped" href="views/admin/uploads/<?= $user['cvfile']?>" target="_blank" data-position="top" data-delay="50" data-tooltip="Download Resume"><span class="fa fa-download"></span></a>
                            </div>
                        </div>
                    </div>
                    <div class="col l7 m6 s12 about-data pd-0">
                        <div class="about-desc pd-30">
                            <div class="arrow-box">Hello & Welcome</div>
                            <div class="overview-name">I'm <span><?= $user['name']?></span></div>
                            <div class="overview-title">Software Engineer </div>
                            <div class="overview-data">
                                <div><span>Age</span><?= $user['age']?></div>
                                <div><span>Address</span><?= $user['address']?></div>
                                <div><span>Email</span><?= $user['email']?></div>
                                <div><span>Phone</span><?= $user['phone']?></div>
                                <div><span></span>017039 30610</div>
                                <div><span>Website</span><?= $user['website_url']?></div>
                            </div>
                        </div>
                        <div class="about-social col s12 pd-0">
                            <a class="waves-effect waves-light col s2 pd-0" href="<?= $user['facebook_url']?>"><span class="fa fa-facebook"></span></a>
                            <a class="waves-effect waves-light col s2 pd-0" href="<?= $user['twitter_url']?>"><span class="fa fa-twitter"></span></a>
                            <a class="waves-effect waves-light col s2 pd-0" href="#0"><span class="fa fa-google-plus"></span></a>
                            <a class="waves-effect waves-light col s2 pd-0" href="<?= $user['linkedin_url']?>"><span class="fa fa-linkedin"></span></a>
                            <a class="waves-effect waves-light col s2 pd-0" href="<?= $user['github_url']?>"><span class="fa fa-github"></span></a>
                            <a class="waves-effect waves-light col s2 pd-0" href="<?= $user['skype_url']?>"><span class="fa fa-skype"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ Overview Section -->

<!-- Skill Section -->
<section id="skill-section" class="skill-section">
    <div class="container">
        <div class="row">
            <div class="section-title">
                Skills
            </div>
            <div class="col s12 section-content skill-wrapper w-block z-depth-1 shadow-change pd-50">
                <div class="col l6 m6 s12 skill-desc pdl-0">
                    <p>

                        <div class="progress-bar-wrapper">
                    <p class="progress-text">PHP
                        <span>90%</span>
                    </p>
                    <div class="progress-bar">
                        <span data-percent="90"></span>
                    </div>
                </div>

                </p>

                <p>
                    <div class="progress-bar-wrapper">
                <p class="progress-text">MySQL
                    <span>85%</span>
                </p>
                <div class="progress-bar">
                    <span data-percent="85"></span>
                </div>
            </div>

            </p>
            <p>
                <div class="progress-bar-wrapper">
            <p class="progress-text">Jquery
                <span>80%</span>
            </p>
            <div class="progress-bar">
                <span data-percent="80"></span>
            </div>
        </div>

        </p>
        <p>
            <div class="progress-bar-wrapper">
        <p class="progress-text">vue JS
            <span>50%</span>
        </p>
        <div class="progress-bar">
            <span data-percent="60"></span>
        </div>
    </div>

    </p>
    <div class="progress-bar-wrapper">
        <p class="progress-text">Javascript
            <span>70%</span>
        </p>
        <div class="progress-bar">
            <span data-percent="70"></span>
        </div>
    </div>
    <p>

    </p>
    </div>
    <div class="col l6 m6 s12 skill-data pdr-0">


        <div class="progress-bar-wrapper">
            <p class="progress-text">Laravel
                <span>75%</span>
            </p>
            <div class="progress-bar">
                <span data-percent="75"></span>
            </div>
        </div>
        <div class="progress-bar-wrapper">
            <p class="progress-text">WordPress
                <span>40%</span>
            </p>
            <div class="progress-bar">
                <span data-percent="40"></span>
            </div>
        </div>
        <div class="progress-bar-wrapper">
            <p class="progress-text">Bootstrap
                <span>80%</span>
            </p>
            <div class="progress-bar">
                <span data-percent="80"></span>
            </div>
        </div>
        <div class="progress-bar-wrapper">
            <p class="progress-text">UI/UX Design
                <span>50%</span>
            </p>
            <div class="progress-bar">
                <span data-percent="50"></span>
            </div>
        </div>
        <div class="progress-bar-wrapper">
            <p class="progress-text">HTML5
                <span>95%</span>
            </p>
            <div class="progress-bar">
                <span data-percent="95"></span>
            </div>
        </div>
        <div class="progress-bar-wrapper">
            <p class="progress-text">CSS3
                <span>90%</span>
            </p>
            <div class="progress-bar">
                <span data-percent="90"></span>
            </div>
        </div>

    </div>
    </div>
    </div>
    </div>
</section>
<!--/ Skill Section -->

<!-- Education Section -->
<section id="education-section" class="education-section">
    <div class="container">
        <div class="row">
            <div class="section-title">
                Education
            </div>
            <div class="col s12 section-content pd-0">
                <ul class="timeline">
                    <li>
                        <div class="timeline-badge">
                            <a><i class="fa fa-circle"></i></a>
                        </div>
                        <div class="timeline-panel w-block z-depth-1 shadow-change pd-30">
                            <div class="timeline-title">
                                BS in Computer Science & Engineering
                            </div>
                            <div class="timeline-tag">
                                International Islamic University Chittagong
                            </div>
                            <div class="timeline-time">2014-2017</div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-badge">
                            <a><i class="fa fa-circle invert"></i></a>
                        </div>
                        <div class="timeline-panel w-block z-depth-1 shadow-change pd-30">
                            <div class="timeline-title">
                                Certification in PHP & MySQL
                            </div>
                            <div class="timeline-tag">
                                Institute of - BASIS Institute of Technology & Management
                            </div>
                            <div class="timeline-time">2016-2017</div>
                        </div>
                    </li>
                    <li>
                        <div class="timeline-badge">
                            <a><i class="fa fa-circle"></i></a>
                        </div>
                        <div class="timeline-panel w-block z-depth-1 shadow-change pd-30">
                            <div class="timeline-title">
                                Certification in Advance PHP & Laravel
                            </div>
                            <div class="timeline-tag">
                                Institute of LICT
                            </div>
                            <div class="timeline-time">2017-2018</div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-badge">
                            <a><i class="fa fa-circle invert"></i></a>
                        </div>
                        <div class="timeline-panel w-block z-depth-1 shadow-change pd-30">
                            <div class="timeline-title">
                                Certification in UI/UX Design
                            </div>
                            <div class="timeline-tag">
                                Institute of shikhbeshobai
                            </div>
                            <div class="timeline-time">2018-2018</div>
                        </div>
                    </li>
                    <li>
                        <div class="timeline-badge">
                            <a><i class="fa fa-circle"></i></a>
                        </div>
                        <div class="timeline-panel w-block z-depth-1 shadow-change pd-30">
                            <div class="timeline-title">
                                Certification in Graphics Design
                            </div>
                            <div class="timeline-tag">
                                Institute of shikhbeshobai
                            </div>
                            <div class="timeline-time">2018-2018</div>
                        </div>
                    </li>
                    <li class="clearfix no-float"></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--/ Education Section -->

<!-- Experience Section -->
<section id="experience-section" class="experience-section">
    <div class="container">
        <div class="row">
            <div class="section-title">
                Experience
            </div>
            <div class="col s12 section-content pd-0">
                <ul class="timeline">
                    <li>
                        <div class="timeline-badge">
                            <a><i class="fa fa-circle"></i></a>
                        </div>
                        <div class="timeline-panel w-block z-depth-1 shadow-change pd-30">
                            <div class="timeline-title">
                                Software Engineer Intern
                            </div>
                            <div class="timeline-tag">
                                Code Station - IT Training & Development
                            </div>
                            <div class="timeline-desc">
                                <p></p>
                            </div>
                            <div class="timeline-time">2018</div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-badge">
                            <a><i class="fa fa-circle invert"></i></a>
                        </div>
                        <div class="timeline-panel w-block z-depth-1 shadow-change pd-30">
                            <div class="timeline-title">
                                Jr. Software Engineer
                            </div>
                            <div class="timeline-tag">
                                Code Station - IT Training & Development
                            </div>
                            <div class="timeline-desc">
                                <p></p>
                            </div>
                            <div class="timeline-time">2018</div>
                        </div>
                    </li>
                    <li class="clearfix no-float"></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--/ Experience Section -->

<!-- Portfolio Section -->
<section id="portfolio-section" class="portfolio-section">
    <div class="container">
        <div class="row">
            <div class="section-title">
                Portfolio
            </div>
            <div class="col s12 section-content pd-0">
                <ul class="filter">
                    <li><a class="active" href="#" data-filter="*">All</a></li>
                    <li><a href="#" data-filter=".design">Laravel Project</a></li>
                    <li><a href="#" data-filter=".development">PHP & mysql Project</a></li>
                    <li><a href="#" data-filter=".branding">WordPress Project</a></li>
                    <li><a href="#" data-filter=".photography">UI Design</a></li>
                </ul>

                <ul class="portfolio-items">
                    <li id="portfolio-1" class="design branding portfolio-content z-depth-1 shadow-change">
                        <figure>
                            <img src="assets/font-end/img/portfolio/p-1.jpg" alt="image">
                            <figcaption>
                                <div class="portfolio-intro">
                                    <div class="portfolio-intro-link">
                                        <a href="single-portfolio.html"><span><i class="fa fa-link"></i></span></a>
                                    </div>
                                    <div class="portfolio-intro-title">
                                        Title Goes Here
                                    </div>
                                    <div class="portfolio-intro-category">
                                        <p>Design</p>
                                        <p>Branding</p>
                                    </div>
                                </div>
                            </figcaption>
                        </figure>
                    </li>

                    <li id="portfolio-2" class="development photography portfolio-content z-depth-1 shadow-change">
                        <figure>
                            <img src="assets/font-end/img/portfolio/p-2.jpg" alt="image">
                            <figcaption>
                                <div class="portfolio-intro">
                                    <div class="portfolio-intro-link">
                                        <a  href="assets/font-end/img/portfolio/p-2.jpg" class="portfolio-mfp"><span><i class="fa fa-search"></i></span></a>
                                    </div>
                                    <div class="portfolio-intro-title">
                                        Title Goes Here
                                    </div>
                                    <div class="portfolio-intro-category">
                                        <p>Development</p>
                                        <p>Photography</p>
                                    </div>
                                </div>
                            </figcaption>
                        </figure>
                    </li>

                    <li id="portfolio-3" class="design development branding portfolio-content z-depth-1 shadow-change">
                        <figure>
                            <img src="assets/font-end/img/portfolio/p-3.jpg" alt="image">
                            <figcaption>
                                <div class="portfolio-intro">
                                    <div class="portfolio-intro-link">
                                        <a  href="https://vimeo.com/108425305" class="video-mfp"><span class="fa fa-video-camera"></span></a>
                                    </div>
                                    <div class="portfolio-intro-title">
                                        Title Goes Here
                                    </div>
                                    <div class="portfolio-intro-category">
                                        <p>Design</p>
                                        <p>Development</p>
                                        <p>Branding</p>
                                    </div>
                                </div>
                            </figcaption>
                        </figure>
                    </li>

                    <li id="portfolio-4" class="design photography portfolio-content z-depth-1 shadow-change">
                        <figure>
                            <img src="assets/font-end/img/portfolio/p-4.jpg" alt="image">
                            <figcaption>
                                <div class="portfolio-intro">
                                    <div class="portfolio-intro-link">
                                        <a  href="assets/font-end/img/portfolio/p-4.jpg" class="portfolio-mfp"><span class="fa fa-search"></span></a>
                                    </div>
                                    <div class="portfolio-intro-title">
                                        Title Goes Here
                                    </div>
                                    <div class="portfolio-intro-category">
                                        <p>Branding</p>
                                        <p>Photography</p>
                                    </div>
                                </div>
                            </figcaption>
                        </figure>
                    </li>

                    <li id="portfolio-5" class="development portfolio-content z-depth-1 shadow-change">
                        <figure>
                            <img src="assets/font-end/img/portfolio/p-5.jpg" alt="image">
                            <figcaption>
                                <div class="portfolio-intro">
                                    <div class="portfolio-intro-link">
                                        <a href="single-portfolio.html"><span><i class="fa fa-link"></i></span></a>
                                    </div>
                                    <div class="portfolio-intro-title">
                                        Title Goes Here
                                    </div>
                                    <div class="portfolio-intro-category">
                                        <p>Development</p>
                                    </div>
                                </div>
                            </figcaption>
                        </figure>
                    </li>

                    <li id="portfolio-6" class="design photography portfolio-content z-depth-1 shadow-change">
                        <figure>
                            <img src="assets/font-end/img/portfolio/p-6.jpg" alt="image">
                            <figcaption>
                                <div class="portfolio-intro">
                                    <div class="portfolio-intro-link">
                                        <a  href="assets/font-end/img/portfolio/p-6.jpg" class="portfolio-mfp"><span><i class="fa fa-search"></i></span></a>
                                    </div>
                                    <div class="portfolio-intro-title">
                                        Title Goes Here
                                    </div>
                                    <div class="portfolio-intro-category">
                                        <p>Branding</p>
                                        <p>Photography</p>
                                    </div>
                                </div>
                            </figcaption>
                        </figure>
                    </li>

                </ul>

            </div>
        </div>
    </div>
</section>
<!--/ Portfolio Section -->

<!-- Testimonial Section -->

<!--
<section id="testimonial-section" class="testimonial-section">
    <div class="container">
        <div class="row">
            <div class="section-title">
                Testimonials
            </div>
            <div class="col l4 m12 s12 testimonial-wrapper pdl-0">
                <div class="col s12 w-block z-depth-1 shadow-change pd-0">
                    <div class="testimonial-content">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.
                        </p>
                    </div>
                    <div class="testimonial-data">
                        <div class="testimonial-name">
                            John Doe
                        </div>
                        <div class="testimonial-title">
                            ABC Inc.
                        </div>
                        <div class="testimonial-img">
                            <img class=" z-depth-1" src="assets/font-end/img/testimonial/t-1.jpg" alt="Testimonial"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col l4 m12 s12 testimonial-wrapper">
                <div class="col s12 w-block z-depth-1 shadow-change pd-0">
                    <div class="testimonial-content">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.
                        </p>
                    </div>
                    <div class="testimonial-data">
                        <div class="testimonial-name">
                            John Doe
                        </div>
                        <div class="testimonial-title">
                            ABC Inc.
                        </div>
                        <div class="testimonial-img">
                            <img class=" z-depth-1" src="assets/font-end/img/testimonial/t-2.jpg" alt="Testimonial"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col l4 m12 s12 testimonial-wrapper pdr-0">
                <div class="col s12 w-block z-depth-1 shadow-change pd-0">
                    <div class="testimonial-content">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.
                        </p>
                    </div>
                    <div class="testimonial-data">
                        <div class="testimonial-name">
                            John Doe
                        </div>
                        <div class="testimonial-title">
                            ABC Inc.
                        </div>
                        <div class="testimonial-img">
                            <img class=" z-depth-1" src="assets/font-end/img/testimonial/t-3.jpg" alt="Testimonial"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

-->
<!--/ Testimonial Section -->



<!-- Blog Section -->
<!--
<section id="blog-section" class="blog-section">
    <div class="container">
        <div class="row">
            <div class="section-title">
                Blog
            </div>
            <div class="col s12 blog-wrapper w-block z-depth-1 shadow-change pd-0">
                <div class="col l6 m6 s12 blog-img pd-0 image-bg" data-image-bg="assets/font-end/img/blog/b-1.jpg"></div> <
                <div class="col m6 s12 l6 blog-desc pd-30">
                    <div class="blog-title">
                        <a href="#0">Here Goes An Awesome Blog Title</a>
                    </div>
                    <div class="blog-data">
                        <a href="#0" class="waves-effect"><span class="fa fa-user"></span>John Doe</a>
                        <a href="#0" class="waves-effect"><span class="fa fa-calendar"></span>Apr 7</a>
                        <a href="#0" class="waves-effect"><span class="fa fa-heart"></span>32</a>
                    </div>
                    <div class="blog-content">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </p>
                    </div>
                    <div class="blog-more">
                        <a href="views/font-end/single-blog.php" class="btn waves-effect waves-light">Read More</a>
                    </div>
                </div>
            </div>
            <div class="col s12 blog-wrapper w-block z-depth-1 shadow-change pd-0">
                <div class="col l6 m6 s12 blog-img pd-0 image-bg" data-image-bg="assets/font-end/img/blog/b-2.jpg"></div>
                <div class="col l6 m6 s12 blog-desc pd-30">
                    <div class="blog-title">
                        <a href="#0">Here Goes An Awesome Blog Title</a>
                    </div>
                    <div class="blog-data">
                        <a href="#0" class="waves-effect"><span class="fa fa-user"></span>John Doe</a>
                        <a href="#0" class="waves-effect"><span class="fa fa-calendar"></span>Apr 7</a>
                        <a href="#0" class="waves-effect"><span class="fa fa-heart"></span>32</a>
                    </div>
                    <div class="blog-content">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </p>
                    </div>
                    <div class="blog-more">
                        <a href="single-blog.php" class="btn waves-effect waves-light">Read More</a>
                    </div>
                </div>
            </div>
            <div class="col s12 blog-wrapper w-block z-depth-1 shadow-change pd-0">
                <div class="col l6 m6 s12 blog-img pd-0 image-bg" data-image-bg="assets/font-end/img/blog/b-3.jpg"></div>
                <div class="col l6 m6 s12 blog-desc pd-30">
                    <div class="blog-title">
                        <a href="#0">Here Goes An Awesome Blog Title</a>
                    </div>
                    <div class="blog-data">
                        <a href="#0" class="waves-effect"><span class="fa fa-user"></span>John Doe</a>
                        <a href="#0" class="waves-effect"><span class="fa fa-calendar"></span>Apr 7</a>
                        <a href="#0" class="waves-effect"><span class="fa fa-heart"></span>32</a>
                    </div>
                    <div class="blog-content">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </p>
                    </div>
                    <div class="blog-more">
                        <a href="single-blog.php" class="btn waves-effect waves-light">Read More</a>
                    </div>
                </div>
            </div>
            <div class="col s12 blog-all mgt-30">
                <a href="views/font-end/blog.php" class="btn waves-effect waves-light">All Blog Posts</a>
            </div>
        </div>
    </div>
</section>

-->
<!--/ Blog Section -->

<!-- Contact Section -->
<section id="contact-section" class="contact-section">
    <div class="container">
        <div class="row">
            <div class="section-title">
                Contact
            </div>
            <div class="col l5 m5 s12 contact-data pdl-0">
                <div class="col s12 w-block z-depth-1 shadow-change pd-0">
                    <div class="col s12 pdt-30 pdr-30 pdb-10 pdl-30">
                        <div class="arrow-box">Useful Info</div>
                        <div class="c-info">
                            <span class="fa fa-phone"></span>
                            <span><?= $user['phone'];?></span>
                        </div>
                        <div class="c-info">
                            <span class="fa fa-road"></span>
                            <span><?= $user['address'];?></span>
                        </div>
                        <div class="c-info">
                            <span class="fa fa-envelope"></span>
                            <span><?= $user['email'];?></span>
                        </div>
                        <div class="c-info">
                            <span class="fa fa-link"></span>
                            <span><?= $user['website_url'];?></span>
                        </div>
                    </div>
                    <div class="col s12 g-map-wrapper pd-0">
                        <div id="g-map" data-latitude="51.5255069" data-longitude="-0.0836207"></div>
                    </div>
                    <div class="col s12 contact-map-btn z-depth-1 shadow-change waves-effect waves-light pd-0">
                        <span class="fa fa-map-marker"></span>View Map
                    </div>
                </div>
            </div>
            <div class="col l7 m7 s12 contact-form pdr-0">
                <div class="col s12 w-block z-depth-1 shadow-change pdt-10 pdr-30 pdb-30 pdl-30">
                    <form id="c-form" class="c-form" action="#0" method="post">
                        <fieldset>
                            <div class="input-field">
                                <input id="name" type="text" name="name" class="validate">
                                <label for="name">Name</label>
                            </div>
                            <div class="input-field">
                                <input id="email" type="email" name="email" class="validate">
                                <label for="email">Email</label>
                            </div>
                            <div class="input-field">
                                <textarea id="message" name="message" class="materialize-textarea validate"></textarea>
                                <label for="message">Message</label>
                            </div>
                            <div>
                                <button class="btn waves-effect waves-light" href="#" type="submit" name="button">Send Message</button>
                                <span id="c-form-status" class="hidden"></span>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ Contact Section -->

<!-- Footer Section -->
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col s12">
                <a class="btn btn-floating waves-effect waves-light back-to-top animatescroll-link" onclick="$('html').animatescroll();" href="#0">
                    <span class="fa fa-angle-up"></span>
                </a>
                <div class="social-links">
                    <a class="waves-effect waves-light" href="<?= $user['facebook_url']?>"><span class="fa fa-facebook"></span></a>
                    <a class="waves-effect waves-light" href="<?= $user['twitter_url']?>"><span class="fa fa-twitter"></span></a>
                    <a class="waves-effect waves-light" href="#"><span class="fa fa-google-plus"></span></a>
                    <a class="waves-effect waves-light" href="<?= $user['linkedin_url']?>"><span class="fa fa-linkedin"></span></a>
                    <a class="waves-effect waves-light" href="<?= $user['github_url']?>"><span class="fa fa-github"></span></a>
                    <a class="waves-effect waves-light" href="<?= $user['skype_url']?>"><span class="fa fa-skype"></span></a>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--/ Footer Section -->

<!-- Color Scheme -->
<div class="color-scheme-select">
    <div class="color-scheme-title">
        20 Awesome Colors
    </div>
    <div id="color-1" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#0EB57D"></div>
    <div id="color-2" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#2196F3"></div>
    <div id="color-3" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#FF1902"></div>
    <div id="color-4" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#FF9800"></div>
    <div id="color-5" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#E91E63"></div>
    <div id="color-6" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#009688"></div>
    <div id="color-7" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#FF5722"></div>
    <div id="color-8" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#9EC139"></div>
    <div id="color-9" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#9C27B0"></div>
    <div id="color-10" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#4CAF50"></div>
    <div id="color-11" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#795548"></div>
    <div id="color-12" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#FF007F"></div>
    <div id="color-13" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#673AB7"></div>
    <div id="color-14" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#8BC34A"></div>
    <div id="color-15" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#3E2723"></div>
    <div id="color-16" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#FF7711"></div>
    <div id="color-17" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#BF9C4F"></div>
    <div id="color-18" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#33691E"></div>
    <div id="color-19" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#607D8B"></div>
    <div id="color-20" class="color-scheme-content w-block z-depth-1 shadow-change waves-effect waves-light" style="background:#FF7077"></div>
    <div class="color-scheme-select-btn">
        <span class="fa fa-cog"></span>
    </div>
</div>
<!-- Color Scheme -->


<?php
include_once 'views/font-end/includes/footer.php';
?>
